const io = require("socket.io");
const server = io.listen(3000);

server.on("connection", socket => {

  console.log("New user connected.");

  socket.on('message', message => {
    socket.broadcast.emit("message_received", message);
  });
});