﻿using SocketIOClient;
using SocketIOClient.Arguments;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var client = new SocketIO("http://localhost:3000");
            client.OnConnected += Client_OnConnected;
            client.OnClosed += Client_OnClosed;
            client.On("message_received", Client_MessageReceived);

            await client.ConnectAsync();

            while(true)
            {
                Console.WriteLine("Enter message.");
                string message = Console.ReadLine();
                await client.EmitAsync("message", message);
            }
        }

        private static void Client_MessageReceived(ResponseArgs responseArgs)
        {
            Console.WriteLine(responseArgs.RawText);
        }

        private static void Client_OnClosed(ServerCloseReason obj)
        {
            Console.WriteLine("Disconnected from the server.");
        }

        private static void Client_OnConnected()
        {
            Console.WriteLine("Connected to the server!");
        }
    }
}
